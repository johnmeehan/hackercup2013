#Facebook Hacker Cup - Beautiful strings - John Meehan
def make_alphabet()   #works
	@alphabet = Hash.new
	("a".."z").each_with_index do |letter, alphabet_weight| 
		@alphabet[letter] = alphabet_weight + 1 #as index starts normally at 0!!
	end 
end

def beautifulstrings(filename)
	@resultshash = Hash.new
	f = File.open(filename,'r')
	linenumber = 0  #does it start at 0?
	f.each_line do |line|
		#get beauty count for this string		
		cleanline = line.downcase.gsub(/[\W\d\s]/,'')		
		if !cleanline.empty? 
			linenumber = linenumber + 1
			thislinetotal = 0 # reset and sets line total to 0
			cleanline.each_char do |c| 					
				if @alphabet.has_key?(c)
					thislinetotal = thislinetotal + @alphabet[c]  
				else
					puts "This character #{c} is not in the alphabet Hash!"
				end
			@resultshash[linenumber] = thislinetotal
			end			
		end 
	end
end

filename = ARGV[0]
make_alphabet
beautifulstrings(filename)

@resultshash.each_pair do |key,value|
	puts "Case ##{key}: #{value}"
end
