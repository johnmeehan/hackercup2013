#Facebook Hacker Cup - Balanced Smileys - John Meehan
def test1?(message)
	#message.empty? #An empty string ""
	if /^$/.match(message).nil?
		false
	else
		true
	end
end

def test2?(message)
	#^([a-zA-Z]+|[\s]{1}|[:]{1})
	if /^([a-zA-Z]+$|^[\s]{1}$|^[:]{1}$)/.match(message).nil? #One or more of the following characters: 'a' to 'z', ' ' (a space) or ':' (a colon)
		false
	else
		true
	end
end

def test3?(message)
	if /^\(+.*\)$/.match(message).nil? #An open parenthesis '(', followed by a message with balanced parentheses, followed by a close parenthesis ')'.
		false
	else
		true
	end
end

def test4?(message)
	#A message with balanced parentheses followed by another message with balanced parentheses. /.*\(+.*\)+.*\(+.*\)+.*/
	if /^[^\)]*\({1}.*\){1}.*\({1}.*\){1}/.match(message).nil?
		false
	else
		true
	end
end
def test5?(message)
	#A smiley face ":)" or a frowny face ":("    old:/.*(:\)|\:\()+.*/
	if /[\(]?(:\)|:\()+[\)]?[^\(]/.match(message).nil?
		false
	else
		true
	end
end

def balanced?(message)
	#is it balanced?
	test1?(message) || test2?(message) || test3?(message) || test4?(message) || test5?(message)
end

def balancedsmileys(filename)
	@resultshash = Hash.new
	f = File.open(filename,'r')
	linenumber = 0  #does it start at 0?
	f.each_line do |line|
		if /^\d+$/.match(line) != nil  #if its not just a number on its own do
			#do nothing its only a a number on its own ha ha ha 
		else
			linenumber = linenumber + 1
			if balanced?(line)
				@resultshash[linenumber] = 'YES'
			else
				@resultshash[linenumber] = 'NO'
			end			
		end 
	end
end

filename = ARGV[0]

balancedsmileys(filename)

@resultshash.each_pair do |key,value|
	puts "Case ##{key}: #{value}"
end
