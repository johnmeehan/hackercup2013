#Facebook Hackercup 2013 -Instructions
Read the problem, paying special attention to the constraints on the input.
Write a program to solve the problem. You can use any programming language that has a free compiler or interpreter available. Your program will need to solve the input file in less than 6 minutes.
Make sure that your program exactly follows the input and output formats. When you run your program on the sample input, it should produce the sample output.
When you are confident that your program is correct, download the input file. This will start a 6-minute timer. You must upload your output and your source code before the timer ends to get credit for the problem.
If you accidentally submit the wrong files or you realize that there is a problem with your program, you can resubmit as many times as you need to during the 6 minutes. Once the 6 minutes has ended, you may not submit again for that problem. The last submission within the 6 minutes will be used for grading.
